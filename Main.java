import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        int randomNumber = random.nextInt(0, 3);
        int[] numbers = {1939,1991,24};
        String[] questions = {"When did the World War II begin?","When did Ukraine become independent?","How many regions are in Ukraine?"};
        System.out.println("What is your name?");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        System.out.println("Let the game begin!");
        int counter = 0;
        int[] numbersOfPlayer = new int[50];
        while (true) {
            System.out.println(questions[randomNumber]);
            System.out.println("Insert the number, please!");
            int numberOfPlayer = scanner.nextInt();
            numbersOfPlayer[counter] = numberOfPlayer;
            counter++;
            if (numberOfPlayer < numbers[randomNumber]) {
                System.out.println("Your number is too small. Please, try again.");
            } else if (numberOfPlayer > numbers[randomNumber]) {
                System.out.println("Your number is too big. Please, try again.");
            } else {
                System.out.println("Congratulations, " + name + "!");
                int[] newArray = Arrays.copyOfRange(numbersOfPlayer,0,counter);
                Arrays.sort(newArray);
                System.out.print("Your numbers: ");
                System.out.println(Arrays.toString(newArray));
                break;
            }
        }

    }
}